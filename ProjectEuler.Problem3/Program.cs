﻿using System;
using System.Linq;
using ProjectEuler.Common;

namespace ProjectEuler.Problem3
{
    class Program
    {
        static void Main(string[] args)
        {
            const Int64 num = 600851475143;

            var factors = Factors.Find(num);
            
            var primeFactors = factors.Where(Primes.Is);

            var largest = primeFactors.Max();

            Console.WriteLine("The largest prime factor of {0} is {1}", num, largest);
            Console.ReadKey();

        }
    }
}
