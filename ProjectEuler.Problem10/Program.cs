﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProjectEuler.Common;

namespace ProjectEuler.Problem10
{
    class Program
    {
        static void Main(string[] args)
        {
            const int under = 2000000;

            // we store them as 64's just so .Sum() works later
            var primes = new List<Int64>();
            for (var i = 1; i < under; i++)
            {
                if (i % 100 == 0)
                {
                    Console.WriteLine("{0}", i);
                }

                if (Primes.Is(i))
                {
                    primes.Add(i);
                }
            }

            var sum = primes.Sum();

            Console.WriteLine("There are {0} primes under {1}. Their sum is {2}", primes.Count, under, sum);
            Console.ReadKey();
        }
    }
}
