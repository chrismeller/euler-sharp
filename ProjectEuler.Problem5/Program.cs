﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProjectEuler.Common;

namespace ProjectEuler.Problem5
{
    class Program
    {
        static void Main(string[] args)
        {
            const int divisibleUpTo = 20;

            var num = 0;
            var found = false;
            do
            {
                num++;

                found = true;

                for (var i = 1; i <= divisibleUpTo; i++)
                {
                    if (num % i != 0)
                    {
                        found = false;
                        break;
                    }
                }
            } while (!found);

            Console.WriteLine("The smallest number divisible by all the numbers below {0} is {1}", divisibleUpTo, num);
            Console.ReadKey();
        }

        // this version was my first approach, but it took _way_ too long, and we don't actually need all the factors anyway - it ends up being far faster to check each number manually
        static void MainOld(string[] args)
        {
            const int divisibleUpTo = 20;

            // build the list of numbers we expect it to be divisible by
            var divisibleBy = new List<Int64>();
            for (var i = 1; i <= divisibleUpTo; i++)
            {
                divisibleBy.Add(i);
            }

            var found = false;
            var num = divisibleUpTo - 1;
            do
            {
                num++;

                if (num % 1000 == 0)
                {
                    Console.WriteLine(num);
                }

                var factors = Factors.Find(num);

                // if there aren't as many factors as we need that's simple... no
                if (factors.Count < divisibleBy.Count)
                {
                    continue;
                }

                // does it contain all the ones we care about?
                if (factors.Intersect(divisibleBy).Count() == divisibleBy.Count)
                {
                    found = true;
                }

            } while (found == false);

            Console.WriteLine("The smallest number divisible by all of the numbers between 1 and {0} is {1}", divisibleUpTo, num);
            Console.ReadKey();
        }
    }
}
