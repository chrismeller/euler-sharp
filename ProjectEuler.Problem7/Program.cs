﻿using System;
using ProjectEuler.Common;

namespace ProjectEuler.Problem7
{
    class Program
    {
        static void Main(string[] args)
        {
            // find the 10,001st prime number
            const int whichPrime = 10001;

            var prime = 0;
            var num = 0;
            do
            {
                num++;

                if (Primes.Is(num))
                {
                    prime++;
                }

            } while (prime < whichPrime);

            Console.WriteLine("Prime {0} is {1}", whichPrime, num);
            Console.ReadKey();
        }
    }
}
