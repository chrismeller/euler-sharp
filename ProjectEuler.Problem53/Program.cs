﻿using System;
using System.Numerics;

namespace ProjectEuler.Problem53
{
    class Program
    {
        static void Main(string[] args)
        {
            // r <= n
            // n! = n x (n-1) x ... x 3 x 2 x 1 (factorial product)
            // 0! = 1
            // n = 23, r = 10 == 1144066
            // C = n! / r!(n−r)!

            var matched = 0;
            for (var n = 1; n <= 100; n++)
            {
                for (var r = n; r >= 0; r--)
                {
                    var combinatoric = CalculateCombinatoric(n, r);

                    if (combinatoric > 1000000)
                    {
                        matched++;
                    }
                }
            }

            Console.WriteLine("There are {0} combinatorics greater than 1000000", matched);
            Console.ReadKey();

        }

        private static BigInteger CalculateCombinatoric(int n, int r)
        {
            // n!
            var numerator = CalculateFactorialProduct(n);

            // r!(n-r)!
            var nMinusR = (n - r);
            var nMinusRFactorialProduct = CalculateFactorialProduct(nMinusR);

            var denominator = CalculateFactorialProduct(r) * nMinusRFactorialProduct;

            return numerator / denominator;

        }

        private static BigInteger CalculateFactorialProduct(int n)
        {
            // 0! = 1
            if (n == 0)
            {
                return 1;
            }

            var nFactorialProduct = new BigInteger(1);
            for (var i = n; i >= 1; i--)
            {
                nFactorialProduct = nFactorialProduct * i;
            }

            return nFactorialProduct;
        }
    }
}
