﻿using System;
using System.Linq;
using System.Numerics;

namespace ProjectEuler.Problem20
{
    class Program
    {
        static void Main(string[] args)
        {
            var n = 100;
            var product = new BigInteger(1);
            do
            {
                product = product * n;

                n--;
            } while (n > 0);

            var digits = product.ToString().ToArray().Select(x => Convert.ToInt32(x.ToString()));
            var sum = digits.Sum();

            Console.WriteLine("The product is {0}", product);
            Console.WriteLine("The sum of {0} is {1}", String.Join(" + ", digits), sum);
            Console.ReadKey();
        }
    }
}
