﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectEuler.Common
{
    public class Factors
    {
        public static List<Int64> Find(Int64 ofNum)
        {
            var factors = new HashSet<Int64>();
            for (var i = Math.Round(Math.Sqrt(ofNum)); i > 0; i--)
            {
                if (ofNum % i == 0)
                {
                    factors.Add(Convert.ToInt64(i));
                    factors.Add(Convert.ToInt64(ofNum / i));
                }
            }

            var asList = factors.ToList();

            asList.Sort();

            return asList;
        }

        public static bool Is(Int64 possibleFactor, Int64 ofNum)
        {
            if (ofNum % possibleFactor == 0)
            {
                return true;
            }

            return false;
        }
    }
}