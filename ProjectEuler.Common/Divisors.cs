﻿using System;
using System.Collections.Generic;

namespace ProjectEuler.Common
{
    public class Divisors
    {
        /// <summary>
        /// Divisors are different from factors in that they are the numbers less than n, while factors includes n.
        /// </summary>
        /// <param name="ofNum"></param>
        /// <returns></returns>
        public static List<Int64> Find(Int64 ofNum)
        {
            var factors = Factors.Find(ofNum);
            factors.Remove(ofNum);

            return factors;
        }

        public static bool Is(Int64 possibleDivisor, Int64 ofNum)
        {
            if (possibleDivisor < ofNum && ofNum % possibleDivisor == 0)
            {
                return true;
            }

            return false;
        }
    }
}