﻿using System;

namespace ProjectEuler.Common
{
    public class Parity
    {
        public static bool IsEven(Int64 num)
        {
            if (num % 2 == 0)
            {
                return true;
            }

            return false;
        }

        public static bool IsOdd(Int64 num)
        {
            return !IsEven(num);
        }
    }
}