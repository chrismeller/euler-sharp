﻿using System;

namespace ProjectEuler.Common
{
    public class Primes
    {
        public static bool Is(Int64 possiblePrime)
        {
            // a prime is a number that is only divisible by itself and 1
            var factors = Factors.Find(possiblePrime);

            if (factors.Count == 2 && factors.Contains(1) && factors.Contains(possiblePrime))
            {
                return true;
            }

            return false;
        }

        public static bool Is(int possiblePrime)
        {
            return Is(Convert.ToInt64(possiblePrime));
        }
    }
}