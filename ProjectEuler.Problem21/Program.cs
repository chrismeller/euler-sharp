﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProjectEuler.Common;

namespace ProjectEuler.Problem21
{
    class Program
    {
        static void Main(string[] args)
        {
            const int under = 10000;

            var amicables = new HashSet<Int64>();
            for (var i = under; i > 0; i--)
            {
                if (amicables.Contains(i)) continue;

                var amicable = FindAmicable(i);

                if (amicable.HasValue)
                {
                    amicables.Add(i);
                    amicables.Add(amicable.Value);
                    Console.WriteLine("The amicalbe number of {0} is {1}", i, amicable.Value);
                }
            }

            var sum = amicables.Sum();

            Console.WriteLine("There are {0} amicable numbers under {1}.", amicables.Count, under);
            Console.WriteLine("{0} = {1}", String.Join(" + ", amicables), sum);
            Console.ReadKey();
        }

        public static Int64? FindAmicable(Int64 n)
        {
            var nDivisors = Divisors.Find(n);
            var nSum = nDivisors.Sum();

            // a != b
            if (nSum == n) return null;

            var dDivisors = Divisors.Find(nSum);
            var dSum = dDivisors.Sum();

            if (dSum == n)
            {
                return nSum;
            }

            return null;
        }
    }
}
