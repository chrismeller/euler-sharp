﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProjectEuler.Common;

namespace ProjectEuler.Problem23
{
    class Program
    {
        static void Main(string[] args)
        {
            const int max = 28123;

            // first we find all the abundant numbers under the max
            var abundants = new List<int>();
            for (var i = 1; i <= max; i++)
            {
                if (IsAbundant(i))
                {
                    abundants.Add(i);
                }
            }

            // now we go back through and find all the numbers that can be written as the sum of two of those
            var abundantSums = new HashSet<int>();
            foreach (var abundant in abundants)
            {
                foreach (var addTo in abundants)
                {
                    var abundantSum = abundant + addTo;

                    abundantSums.Add(abundantSum);
                }
            }

            // and finally, find all the numbers that aren't in that set
            var nonAbundantSums = new List<int>();
            for (var i = 1; i <= max; i++)
            {
                if (abundantSums.Contains(i) == false)
                {
                    nonAbundantSums.Add(i);
                }
            }

            var sum = nonAbundantSums.Sum();

            Console.WriteLine("There are {0} abundant numbers <= {1}", abundants.Count, max);
            Console.WriteLine("There are {0} numbers that can be created as the sum of those abundant numbers.", abundantSums.Count);
            Console.WriteLine("There are {0} numbers which cannot be created by summing two abundant numbers and their sum is {1}", nonAbundantSums.Count, sum);
            Console.ReadKey();
        }

        public static bool IsAbundant(int num)
        {
            var divisors = Divisors.Find(num);

            var sum = divisors.Sum();

            if (sum > num)
            {
                return true;
            }

            return false;
        }
    }
}
