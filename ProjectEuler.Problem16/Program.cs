﻿using System;
using System.Linq;
using System.Numerics;

namespace ProjectEuler.Problem16
{
    class Program
    {
        static void Main(string[] args)
        {
            var targetDouble = Math.Pow(2, 1000);
            var target = new BigInteger(targetDouble);

            var digits = target.ToString().ToArray().Select(x => Convert.ToInt64(x.ToString())).ToList();
            var sum = digits.Sum();

            var digitsStr = String.Join(" + ", digits);
            Console.WriteLine("{0} = {1}", digitsStr, sum);
            Console.ReadKey();
        }
    }
}
