﻿using System;
using System.Linq;

namespace ProjectEuler.Problem52
{
    class Program
    {
        static void Main(string[] args)
        {
            Int64 n = 0;
            var allMatch = false;
            do
            {
                n++;

                var x2 = n * 2;
                var x3 = n * 3;
                var x4 = n * 4;
                var x5 = n * 5;
                var x6 = n * 6;

                var match2 = IsMatch(n, x2);
                var match3 = IsMatch(n, x3);
                var match4 = IsMatch(n, x4);
                var match5 = IsMatch(n, x5);
                var match6 = IsMatch(n, x6);

                allMatch = (match2 && match3 && match4 && match5 && match6);
            } while (allMatch == false);

            Console.WriteLine("The smallest number that matches is {0}", n);
            Console.ReadKey();
        }

        public static bool IsMatch(Int64 one, Int64 two)
        {
            var digitsOne = one.ToString().ToCharArray().Select(x => Convert.ToInt64(x.ToString())).ToList();
            var digitsTwo = two.ToString().ToCharArray().Select(x => Convert.ToInt64(x.ToString())).ToList();

            digitsOne.Sort();
            digitsTwo.Sort();

            // if they aren't the same length, obviously they're not a match
            if (digitsOne.Count != digitsTwo.Count) return false;

            for (var i = 0; i < digitsOne.Count; i++)
            {
                if (digitsOne[i] != digitsTwo[i])
                {
                    return false;
                }
            }

            return true;
        }
    }
}
