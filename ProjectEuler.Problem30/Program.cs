﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectEuler.Problem30
{
    class Program
    {
        static void Main(string[] args)
        {
            const int min = 2;
            const int max = 9999999;
            const int toPower = 5;

            var matching = new List<int>();
            for (var i = min; i <= max; i++)
            {
                var digits = i.ToString().ToCharArray().Select(x => Convert.ToInt32(x.ToString())).ToList();

                var pows = new List<int>();
                foreach (var digit in digits)
                {
                    var pow = Convert.ToInt32(Math.Pow(digit, toPower));

                    pows.Add(pow);
                }

                var powsSum = pows.Sum();

                if (powsSum == i)
                {
                    matching.Add(i);
                }
            }

            var sum = matching.Sum();

            Console.WriteLine("There were {0} values that matched. Their sum is {1}", matching.Count, sum);
            Console.ReadKey();
        }
    }
}
