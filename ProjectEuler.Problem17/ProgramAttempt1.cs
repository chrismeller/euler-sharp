﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectEuler.Problem17
{
    /// <summary>
    /// this was my first attempt - stringify the given number so we can iterate over it. it ended up being needlessly complex when determining the positioning of the "and"
    /// 
    /// of course 
    /// </summary>
    class ProgramAttempt1
    {
        private static List<string> _ones = new List<string>()
        {
            "one",
            "two",
            "three",
            "four",
            "five",
            "six",
            "seven",
            "eight",
            "nine",
            "ten"
        };

        private static List<string> _teens = new List<string>()
        {
            "eleven",
            "twelve",
            "thirteen",
            "fourteen",
            "fifteen",
            "sixteen",
            "seventeen",
            "eighteen",
            "nineteen"
        };

        private static List<string> _tens =
            new List<string>() {"twenty", "thirty", "fourty", "fifty", "sixty", "seventy", "eighty", "ninety"};

        static void MainAttempt1(string[] args)
        {
            const int target = 5399;

            var asString = Stringify(target);

            Console.WriteLine("{0} = {1}", target, asString);
            Console.ReadKey();
        }

        private static string Stringify(int num)
        {
            var pieces = new List<string>();

            var n = num;
            if (n >= 1000)
            {
                var thousands = Convert.ToInt32(Math.Floor((n / 1000M)));

                var word = _ones[thousands - 1];

                pieces.Add(String.Format("{0} thousand", word));

                n = n - (thousands * 1000);
            }

            if (n >= 100)
            {
                var hundreds = Convert.ToInt32(Math.Floor((n / 100M)));

                var word = _ones[hundreds - 1];

                pieces.Add(String.Format("{0} hundred", word));

                n = n - (hundreds * 100);
            }

            if (n >= 20)
            {
                var tens = Convert.ToInt32(Math.Floor((n / 10M)));

                var word = _tens[tens - 2];

                pieces.Add(word);

                n = n - (tens * 10);
            }

            if (n > 10)
            {
                var teens = n - 10;

                var word = _teens[teens - 1];

                pieces.Add(word);

                n = n - teens - 10;
            }

            if (n >= 1)
            {
                var ones = n;

                var word = _ones[ones - 1];

                pieces.Add(word);

                n = n - ones;
            }

            var fullString = "";

            // we only need to include "and" if the number was over 100
            if (num > 100)
            {
                // but if it had a tens place we include that and the ones place after the and
                if (num >= 20)
                {
                    
                }
                var lastPiece = pieces.Last();
                pieces.RemoveAt(pieces.Count - 1);

                fullString = String.Join(" ", pieces) + " and " + lastPiece;
            }
            else
            {
                fullString = String.Join(" ", pieces);
            }

            return fullString;
        }
    }
}
