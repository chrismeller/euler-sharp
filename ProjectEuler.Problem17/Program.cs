﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectEuler.Problem17
{
    /// <summary>
    /// See ProgramAttempt1 for my first attempt that ended up needlessly complicated
    /// </summary>
    public class Program
    {
        private static List<string> _ones = new List<string>()
        {
            "one",
            "two",
            "three",
            "four",
            "five",
            "six",
            "seven",
            "eight",
            "nine"
        };

        private static List<string> _teens = new List<string>()
        {
            "ten",
            "eleven",
            "twelve",
            "thirteen",
            "fourteen",
            "fifteen",
            "sixteen",
            "seventeen",
            "eighteen",
            "nineteen"
        };

        private static List<string> _tens =
            new List<string>() { "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

        public static void Main(string[] args)
        {
            // we start out with 1000 - we know it won't be the longest, but it's our cap - no reason to handle it
            var numbers = new List<string>();

            // just add all the ones (four) and teens (fourteen), they have no compound forms
            numbers.AddRange(_ones);
            numbers.AddRange(_teens);

            // now we handle all the tens-ones combos
            foreach (var ten in _tens)
            {
                // fourty
                numbers.Add(ten);

                foreach (var one in _ones)
                {
                    // fourty-four
                    numbers.Add(String.Format("{0}-{1}", ten, one));
                }
            }

            // now add in the hundreds and all its combos
            foreach (var hundred in _ones)
            {
                // four hundred
                numbers.Add(String.Format("{0} hundred", hundred));

                foreach (var one in _ones)
                {
                    // four hundred and four
                    numbers.Add(String.Format("{0} hundred and {1}", hundred, one));
                }

                foreach (var teen in _teens)
                {
                    // four hundred and fourteen
                    numbers.Add(String.Format("{0} hundred and {1}", hundred, teen));
                }

                foreach (var ten in _tens)
                {
                    // four hundred and fourty
                    numbers.Add(String.Format("{0} hundred and {1}", hundred, ten));

                    // four hundred and fourty-four
                    foreach (var one in _ones)
                    {
                        numbers.Add(String.Format("{0} hundred and {1}-{2}", hundred, ten, one));
                    }
                }
            }
            
            // finally, add the top number, we're inclusive
            numbers.Add("one thousand");

            numbers.ForEach(Console.WriteLine);

            // remember, we don't count spaces or hyphens
            var letters = 0;
            //numbers.ForEach(x => letters = letters + x.Replace(" ", "").Replace("-", "").Length);
            foreach (var number in numbers)
            {
                var stripped = number.Replace(" ", "").Replace("-", "");
                letters = letters + stripped.Length;
            }

            Console.WriteLine("There are a total of {0} letters used in the first {1} numbers.", letters, numbers.Count);
            Console.ReadKey();
        }
    }
}