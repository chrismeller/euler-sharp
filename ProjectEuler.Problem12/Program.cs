﻿using System;
using ProjectEuler.Common;

namespace ProjectEuler.Problem12
{
    class Program
    {
        static void Main(string[] args)
        {
            const int expectedNumDivisors = 500;

            var triangleNumber = 0;
            var triangleTotal = 0;
            var numDivisors = 0;
            do
            {
                triangleNumber++;
                triangleTotal = 0;

                for (var i = 1; i <= triangleNumber; i++)
                {
                    triangleTotal = triangleTotal + i;
                }

                var factors = Factors.Find(triangleTotal);

                numDivisors = factors.Count;

            } while (numDivisors < expectedNumDivisors);

            Console.WriteLine("The {0}th triangle number had a total of {1} and is the first triangle number to have {2} divisors.", triangleNumber, triangleTotal, expectedNumDivisors);
            Console.ReadKey();
        }
    }
}
