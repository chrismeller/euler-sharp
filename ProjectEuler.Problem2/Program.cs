﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectEuler.Problem2
{
    class Program
    {
        static void Main(string[] args)
        {
            const int under = 4000000;

            var fibonaccis = new List<int>() {1, 2};

            var lastFibonacci = 1;
            var fibonacci = 2;
            while (fibonacci < under)
            {
                var newFibonacci = lastFibonacci + fibonacci;

                lastFibonacci = fibonacci;
                fibonacci = newFibonacci;

                fibonaccis.Add(newFibonacci);
            }


            var evens = fibonaccis.Where(x => x % 2 == 0);
            var sum = evens.Sum();


            Console.WriteLine("First 10: {0}", String.Join(", ", fibonaccis.Take(10)));
            Console.WriteLine("First 10 evens: {0}", String.Join(", ", evens.Take(10)));
            Console.WriteLine("Sum of evens under {0} = {1}", under, sum);

            Console.ReadKey();
        }
    }
}
