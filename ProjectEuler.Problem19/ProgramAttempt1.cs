﻿using System;
using System.Collections.Generic;

namespace ProjectEuler.Problem19
{
    class ProgramAttempt1
    {
        /// <summary>
        /// This one does actually work, but felt like cheating.
        /// </summary>
        /// <param name="args"></param>
        static void MainAttempt1(string[] args)
        {
            var start = new DateTimeOffset(new DateTime(1901, 1, 1), TimeSpan.FromHours(-5));
            var end = new DateTimeOffset(new DateTime(2000, 12, 31), TimeSpan.FromHours(-5));

            var firstSundays = new List<string>();
            var currentDate = start;
            do
            {
                if (currentDate.Day == 1 && currentDate.DayOfWeek == DayOfWeek.Sunday)
                {
                    firstSundays.Add(currentDate.ToString("R"));
                }

                currentDate = currentDate.AddDays(1);
            } while (currentDate <= end);

            firstSundays.ForEach(Console.WriteLine);
            Console.WriteLine("There were {0} first Sundays.", firstSundays.Count);
            Console.ReadKey();

        }
    }
}
