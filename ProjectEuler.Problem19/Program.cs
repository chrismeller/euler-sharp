﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace ProjectEuler.Problem19
{
    public class Program
    {
        private static Dictionary<int, int> _daysInMonth = new Dictionary<int, int>()
        {
            {1, 31 },
            {2, 28 },
            {3, 31 },
            {4, 30 },
            {5, 31 },
            {6, 30 },
            {7, 31 },
            {8, 31 },
            {9, 30 },
            {10, 31 },
            {11, 30 },
            {12, 31 }
        };

        private static Dictionary<int,string> _months = new Dictionary<int, string>()
        {
            {1, "Jan" },
            {2, "Feb" },
            {3, "Mar" },
            {4, "Apr" },
            {5, "May" },
            {6, "Jun" },
            {7, "Jul" },
            {8, "Aug" },
            {9, "Sep" },
            {10, "Oct" },
            {11, "Nov" },
            {12, "Dec" }
        };

        private static Dictionary<int, string> _daysOfWeek = new Dictionary<int, string>()
        {
            {0, "Monday"},
            {1, "Tuesday"},
            {2, "Wednesday"},
            {3, "Thursday"},
            {4, "Friday"},
            {5, "Saturday"},
            {6, "Sunday"}
        };

        static void Main(string[] args)
        {
            // we're told 1/1/1900 was a monday
            var dayOfWeek = 0;

            var firstSundays = new List<string>();

            for (var year = 1900; year <= 2000; year++)
            {
                for (var month = 1; month <= 12; month++)
                {
                    var daysInMonth = DaysInMonth(year, month);

                    for (var day = 1; day <= daysInMonth; day++)
                    {
                        // if it's sunday AND it's the first of the month
                        if (dayOfWeek == 6 && day == 1 && year > 1900)
                        {
                            firstSundays.Add(String.Format("{0}-{1}-{2}", year, month, day));
                        }

                        // if it's sunday, wrap back around to monday
                        if (dayOfWeek == 6)
                        {
                            dayOfWeek = 0;
                        }
                        else
                        {
                            dayOfWeek++;
                        }
                    }
                }
            }

            firstSundays.ForEach(x =>
            {
                var pieces = x.Split('-');
                var year = pieces[0];
                var month = pieces[1];
                var day = pieces[2];

                var monthStr = _months[Convert.ToInt32(month)];

                Console.WriteLine("{0} {1} {2}", day.PadLeft(2, '0'), monthStr, year.Substring(2));
            });
            Console.WriteLine("There were {0} first sundays.", firstSundays.Count);
            Console.ReadKey();
        }

        private static int DaysInMonth(int year, int month)
        {
            if (month != 2)
            {
                return _daysInMonth[month];
            }

            // if the year is evenly divisible by 4 AND it's divisible by 100 AND it's divisible by 400, this is a century that is a leap year
            if (year % 4 == 0 && year % 100 == 0 && year % 400 == 0)
            {
                return 29;
            }
            // if the year is evenly divisible by 4 AND it's divisible by 100, it's not a leap year
            else if (year % 4 == 0 && year % 100 == 0)
            {
                return 28;
            }
            // if the year is just divisible by 4, it's a leap year
            else if (year % 4 == 0)
            {
                return 29;
            }
            // otherwise it's not a leap year
            else
            {
                return 28;
            }

            // if the year is evenly divisible by 4, it's a leap year
            if (year % 4 == 0)
            {
                // but only if it's not a century, unless it's divisible by 400
                if (year % 100 == 0 && year % 400 == 0)
                {
                    return 29;
                }
                else if (year % 100 == 0)
                {
                    return 28;
                }
                else
                {
                    return 29;
                }
            }
            else
            {
                return 28;
            }
        }
    }
}