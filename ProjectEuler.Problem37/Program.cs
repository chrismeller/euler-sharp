﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProjectEuler.Common;

namespace ProjectEuler.Problem37
{
    class Program
    {
        static void Main(string[] args)
        {
            // single digit primes don't count, start at 8
            var currentNum = 8;
            var truncatablePrimes = new List<int>();
            do
            {
                if (Primes.Is(currentNum) && IsLeftTruncatable(currentNum) && IsRightTruncatable(currentNum))
                {
                    truncatablePrimes.Add(currentNum);
                }

                currentNum++;
            } while (truncatablePrimes.Count < 11);

            var sum = truncatablePrimes.Sum();

            Console.WriteLine("{0} = {1}", String.Join(" + ", truncatablePrimes), sum);
            Console.ReadKey();
        }

        public static bool IsLeftTruncatable(int prime)
        {
            var remainder = prime.ToString();
            do
            {
                if (Primes.Is(Convert.ToInt32(remainder)) == false) return false;

                remainder = remainder.Substring(1);
            } while (remainder.Length > 0);

            return true;
        }

        public static bool IsRightTruncatable(int prime)
        {
            var remainder = prime.ToString();
            do
            {
                if (Primes.Is(Convert.ToInt32(remainder)) == false) return false;
                
                remainder = remainder.Substring(0, remainder.Length - 1);
            } while (remainder.Length > 0);
            
            return true;
        }
    }
}
