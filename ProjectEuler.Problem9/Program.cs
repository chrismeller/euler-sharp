﻿using System;

namespace ProjectEuler.Problem9
{
    class Program
    {
        static void Main(string[] args)
        {
            var goal = 1000;

            var foundA = 0;
            var foundB = 0;
            var foundC = 0;

            var found = false;

            // we use goal as the upper bound simply because a + b + c = 1000, so each of them has to be under 1000 themselves
            for (var a = 1; a <= goal; a++)
            {
                // a < b < c, so we can limit start b and c off with the value of their predecessor
                for (var b = a; b <= goal; b++)
                {
                    for (var c = b; c <= goal; c++)
                    {
                        
                        // if they don't all equal our goal, there's no reason to check to see if they're a triplet anyway
                        if (a + b + c == goal)
                        {
                            Console.WriteLine("{0} + {1} + {2} = {3}", a, b, c, goal);

                            var aSq = Convert.ToInt64(Math.Pow(a, 2));
                            var bSq = Convert.ToInt64(Math.Pow(b, 2));
                            var cSq = Convert.ToInt64(Math.Pow(c, 2));

                            if (aSq + bSq == cSq)
                            {
                                Console.WriteLine("Powers match!");

                                foundA = a;
                                foundB = b;
                                foundC = c;

                                // why the hell doesn't C# support break 3;?
                                found = true;
                            }
                        }

                        if (found) break;

                    }

                    if (found) break;
                }

                if (found) break;
            }

            var product = foundA * foundB * foundC;

            Console.WriteLine("{0}^2 + {1}^2 = {2}^2 and {3} + {4} + {5} = {6}", foundA, foundB, foundC, foundA, foundB, foundC, goal);
            Console.WriteLine("{0} x {1} x {2} = {3}", foundA, foundB, foundC, product);
            Console.ReadKey();
        }
    }
}
