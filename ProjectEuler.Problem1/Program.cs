﻿using System;

namespace ProjectEuler.Problem1
{
    class Program
    {
        static void Main(string[] args)
        {
            const int under = 1000;

            var sum = 0;
            for (var i = 1; i < under; i++)
            {
                if (i % 3 == 0 || i % 5 == 0)
                {
                    sum = sum + i;
                }
            }

            Console.WriteLine("Sum: {0}", sum);
            Console.ReadKey();
        }
    }
}
