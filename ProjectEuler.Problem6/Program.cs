﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectEuler.Problem6
{
    class Program
    {
        static void Main(string[] args)
        {
            const int upTo = 100;
            
            // all the numbers we're going to be working with
            var nums = new List<int>();
            for (var i = 1; i <= upTo; i++)
            {
                nums.Add(i);
            }

            var squares = nums.Select(x => Convert.ToInt32(Math.Pow(x, 2))).ToList();
            var squaresSum = squares.Sum();

            var sums = nums.Sum();
            var sumsSquare = Convert.ToInt32(Math.Pow(sums, 2));

            var diff = sumsSquare - squaresSum;

            Console.WriteLine("The sum of the squares is {0} and the square of the sums is {1}. The difference is {2}", squaresSum, sumsSquare, diff);
            Console.ReadKey();
        }
    }
}
