﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ProjectEuler.Problem22
{
    class Program
    {
        static void Main(string[] args)
        {
            var contents = File.ReadAllText("names.txt");
            var names = contents.Trim('"').Split(new[] {"\",\""}, StringSplitOptions.RemoveEmptyEntries).ToList();
            names.Sort();

            var scores = new List<int>();
            var i = 1;
            foreach (var name in names)
            {
                var alphaValue = CalculateAlphaValue(name);

                var score = alphaValue * i;

                scores.Add(score);

                i++;
            }

            var sum = scores.Sum();

            Console.WriteLine("There were {0} names and their scores total {1}", names.Count, sum);
            Console.ReadKey();
        }

        public static int CalculateAlphaValue(string name)
        {
            var alphaValue = 0;

            // standardize them all to uppercase, which means A is 65
            var letters = name.ToUpper().ToCharArray();
            foreach (var letter in letters)
            {
                var ordinal = (int)letter;

                // A is a decimal 65 in ASCII, so we subtract 64 to make it the first letter
                ordinal = ordinal - 64;

                alphaValue = alphaValue + ordinal;
            }

            return alphaValue;
        }
    }
}
