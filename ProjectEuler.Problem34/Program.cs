﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProjectEuler.Common;

namespace ProjectEuler.Problem34
{
    class Program
    {
        static void Main(string[] args)
        {
            var matching = new List<Int64>();
            for (Int64 i = 3; i < 999999; i++)
            {
                var digits = i.ToString().ToCharArray().Select(x => Convert.ToInt32(x.ToString())).ToList();

                var digitFactorials = new List<Int64>();
                foreach (var digit in digits)
                {
                    Int64 product = 1;
                    for(var n = 1; n <= digit; n++)
                    {
                        product = product * n;
                    }

                    digitFactorials.Add(product);
                }

                var digitFactorialSum = digitFactorials.Sum();

                if (digitFactorials.Count > 1 && digitFactorialSum == i)
                {
                    matching.Add(i);
                }
            }

            var sum = matching.Sum();


            Console.WriteLine("There were {0} digits who are equal to the sum of the factorial of their digits. They are:", matching.Count);
            matching.ForEach(Console.WriteLine);
            Console.WriteLine(" Their sum is {0}", sum);
            Console.ReadKey();
        }
    }
}
