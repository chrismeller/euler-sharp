﻿using System;
using System.Numerics;

namespace ProjectEuler.Problem48
{
    class Program
    {
        static void Main(string[] args)
        {
            const int max = 1000;

            var sum = new BigInteger(0);
            for (var i = 1; i <= max; i++)
            {
                var pow = BigInteger.Pow(i, i);

                sum = sum + pow;
            }

            var asString = sum.ToString();

            var last10 = asString.Substring(asString.Length - 10);

            Console.WriteLine("The last 10 digits of the sum are {0}", last10);
            Console.ReadKey();
        }
    }
}
