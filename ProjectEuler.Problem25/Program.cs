﻿using System;
using System.Collections.Generic;
using System.Numerics;

namespace ProjectEuler.Problem25
{
    class Program
    {
        static void Main(string[] args)
        {
            const int numDigits = 1000;

            var term1 = new BigInteger(1);
            var term2 = new BigInteger(2);

            var index = 3;
            do
            {
                var newTerm = term1 + term2;

                // everything bumps down a level
                term1 = term2;
                term2 = newTerm;

                index++;

            } while (term2.ToString().Length < numDigits);

            Console.WriteLine("The first index to contain {0} digits is {1}", numDigits, index);
            Console.ReadKey();
        }
    }
}
