﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectEuler.Problem36
{
    class Program
    {
        static void Main(string[] args)
        {
            const int max = 1000000;

            var palindromics = new List<int>();
            for (var i = 1; i < max; i++)
            {
                var asBin = Convert.ToString(i, 2);
                if (IsPalindromic(i.ToString()) && IsPalindromic(asBin))
                {
                    Console.WriteLine("{0} and {1} are palindromic!", i, asBin);

                    palindromics.Add(i);
                }
            }

            var sum = palindromics.Sum();

            Console.WriteLine("There were {0} palindromic numbers below {1}. Their sum is {2}", palindromics.Count, max, sum);
            Console.ReadKey();
        }

        public static bool IsPalindromic(string num)
        {
            // first, make sure there aren't any leading 0's, those aren't allowed
            if (num.TrimStart('0').Length != num.Length) return false;

            // i'm not sure if 55 is considered palindromic, but we'll assume not -- they are
            //if (num.Length <= 2) return false;

            var half = Convert.ToInt32(Math.Floor(num.Length / 2M));

            var digits = num.ToCharArray().Select(x => Convert.ToInt32(x.ToString()));

            var firstHalf = String.Join("", digits.Take(half));
            var lastHalf = String.Join("", digits.Reverse().Take(half));

            if (firstHalf == lastHalf)
            {
                return true;
            }

            return false;
        }
    }
}
