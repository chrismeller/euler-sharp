﻿using System;
using System.Collections.Generic;
using ProjectEuler.Common;

namespace ProjectEuler.Problem14
{
    class Program
    {
        static void Main(string[] args)
        {

            //Proof();

            const int under = 1000000;

            var longestChain = new List<Int64>();
            Int64 longestNumber = 0;
            for (Int64 i = 1; i < under; i++)
            {
                var n = i;
                var chain = new List<Int64>() {n};
                do
                {
                    n = Iterate(n);

                    chain.Add(n);
                } while (n > 1);

                if (chain.Count > longestChain.Count)
                {
                    longestChain = chain;
                    longestNumber = i;

                    Console.WriteLine("{0} just produced the longest chain of {1} terms.", longestNumber, longestChain.Count);
                }
            }

            var chainStr = String.Join(" -> ", longestChain);

            Console.WriteLine("{0} produces the longest chain under {1} - {2} terms", longestNumber, under, longestChain.Count);
            Console.WriteLine(chainStr);
            Console.ReadKey();

        }

        private static void Proof()
        {
            var n = 13L;
            var chain = new List<Int64>() {n};    // the chain starts with n itself
            do
            {
                n = Iterate(n);

                chain.Add(n);
            } while (n > 1);

            var chainStr = String.Join(" -> ", chain);

            Console.WriteLine("{0}, {1} terms.", chainStr, chain.Count);
            Console.ReadKey();
        }

        private static Int64 Iterate(Int64 num)
        {
            if (Parity.IsEven(num))
            {
                return num / 2;
            }
            else
            {
                return (3 * num) + 1;
            }
        }
    }
}
