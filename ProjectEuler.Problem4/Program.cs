﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectEuler.Problem4
{
    class Program
    {
        static void Main(string[] args)
        {
            const int start = 100;
            const int stop = 999;

            var largestPalindromic = 0;
            for (var i = start; i <= stop; i++)
            {
                for (var j = start; j <= stop; j++)
                {
                    var product = i * j;

                    if (IsPalindromic(product) && product > largestPalindromic)
                    {
                        largestPalindromic = product;
                    }
                }
            }

            Console.WriteLine("The largest palindromic number between {0} and {1} is {2}", start, stop, largestPalindromic);
            Console.ReadKey();
        }

        public static bool IsPalindromic(Int64 num)
        {
            var asString = num.ToString();

            // we use floor simply because 123454321 is also palindromic, so we need to round down so we only get 1234 / 4321
            var half = Convert.ToInt16(Math.Floor(Convert.ToDecimal(asString.Length) / 2));

            var first = asString.ToArray().Take(half);
            var last = asString.ToArray().Reverse().Take(half);

            if(String.Join("", first) == String.Join("", last))
            {
                return true;
            }

            return false;
        }
    }
}
