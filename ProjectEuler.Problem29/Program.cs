﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace ProjectEuler.Problem29
{
    class Program
    {
        static void Main(string[] args)
        {
            const int min = 2;
            const int max = 100;

            var terms = new HashSet<string>();

            for (var a = min; a <= max; a++)
            {
                for (var b = min; b <= max; b++)
                {
                    var aBI = new BigInteger(a);

                    var term = BigInteger.Pow(a, b);

                    terms.Add(term.ToString());
                }
            }

            Console.WriteLine("There are {0} unique terms for a^b where each value is between {1} and {2}, inclusive.", terms.Count, min, max);
            Console.ReadKey();
        }
    }
}
